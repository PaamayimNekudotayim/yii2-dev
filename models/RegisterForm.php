<?php

namespace app\models;

use Yii;
use yii\base\Model;

class RegisterForm extends Model
{
	/**@var string */
	public $first_name;
	/**@var string */
	public $last_name;
	/**@var string */
	public $login;
	/**@var string */
	public $email;
	/**@var string */
	public $birth_date;
	/**@var string */
	public $gender;
	/**@var string */
	public $type;
	/**@var string */
	public $password;
	/**@var string */
	public $created_at;
	/**@var string */
	public $updated_at;
	
	public $attributes = [
		'first_name' => '',
		'last_name' => '',
		'login' => '',
		'email' => '',
		'birth_date' => '',
		'password' => '',
		'type' => '',
		'created_at' => '',
		'updated_at' => '',
	];
	
	public function rules()
	{
		return [
			[['first_name', 'last_name', 'login', 'email', 'birth_date', 'password'], 'required'],
			['email', 'email'],
			['password', 'string', 'min' => 6]
		];
	}
	
	/**
	 * @param $posted
	 */
	public function loadData($posted)
	{
		foreach ($this->attributes as $key => $value)
		{
			if (isset($posted[$key])) {
				$this->attributes[$key] = $posted[$key];
			}
		}
	}
	
	
	/**
	 * @throws \yii\db\Exception
	 */
	public function checkUnique()
	{
		/**@var User $user*/
		$user =  UserRegister::instance()->checkLoggedInUser('user');
		if ($user) {
			if (!empty($user)) {
				if ($user->login == $this->attributes['login']) {
					Yii::$app->session->setFlash('error', 'Login Already Taken');
				} else if ($user->email == $this->attributes['email']) {
					Yii::$app->session->setFlash('error', 'Email Already Taken');
				} else {
					Yii::$app->session->setFlash('success', 'Successful register');
				}
				
				return false;
			}
		}
		
		return true;
	}
}