<?php

namespace app\models;

use app\controllers\AppSessionController;
use PDO;
use Yii;
use yii\base\StaticInstanceTrait;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

class User
{
	use StaticInstanceTrait;
	
	/**@var $id int*/
	public $id;
	
	/**@var $login string*/
	public $login;
	
	/**@var $email string*/
	public $email;
	
	/**@var $password string*/
	public $password;
	
	/**
	 * @param bool $type
	 * @return array|false
	 * @throws \yii\db\Exception
	 */
	public static function findByUsername($type = false)
	{
		if (true == $type) {
			$query = Yii::$app->db->createCommand("SELECT * FROM user WHERE login=:login AND type = '1' LIMIT 1");
		} else {
			$query = Yii::$app->db->createCommand("SELECT * FROM user WHERE login=:login LIMIT 1");
		}
		
		return $query->bindValues(['login' => $_POST['LoginForm']['login']])->queryOne(PDO::FETCH_OBJ);
	}
	
	/**
	 * @param $password
	 * @param $userPassword
	 * @return bool
	 */
	public function validatePassword($password, $userPassword)
	{
		if (Yii::$app->getSecurity()->validatePassword($password, $userPassword)) {
			return true;
		}
		
		return false;
	}
	
	/**
	 * @return bool
	 */
	public function isAdmin()
	{
		$isAdmin = false;
		
		if (null !== AppSessionController::instance()->get('user')) {
			$isAdmin = AppSessionController::instance()->get('user');
		}
		
		return (isset($isAdmin) && $isAdmin['type'] == '1');
	}
	
	
	/**
	 * @param $table
	 * @return array
	 * @throws \yii\db\Exception
	 */
	public function getRegisteredUsersIfAny($table)
	{
		return Yii::$app->db->createCommand("SELECT id, first_name, last_name, email, type, created_at
		FROM {$table} ORDER BY created_at DESC")->queryAll(PDO::FETCH_OBJ);
	}
	
	/**
	 * @param $table
	 * @param $id
	 * @return array|false
	 * @throws \yii\db\Exception
	 */
	public function selectAuthorToEdit($table, $id)
	{
		$query = Yii::$app->db->createCommand("SELECT id, first_name, last_name, email, type, login
				FROM {$table} WHERE id=:id");
		return $query->bindValues(['id' => $id])->queryOne(PDO::FETCH_OBJ);
	}
	
	
	/**
	 * @param $table
	 * @param $data
	 * @param string $type
	 * @return int
	 * @throws \yii\db\Exception
	 */
	public function updateAuthor($table, $data , $type = '2')
	{
		if ($type == '1') {
			$sql = "UPDATE {$table} SET first_name=:first_name, last_name=:last_name, email=:email, login=:login, type=:type,
                					updated_at = CURRENT_TIMESTAMP() WHERE id=:id";
		} else {
			$sql = "UPDATE {$table} SET first_name=:first_name, last_name=:last_name, email=:email, login=:login,
                					updated_at = CURRENT_TIMESTAMP() WHERE id=:id";
		}
		
		$query = Yii::$app->db->createCommand($sql);
		
		foreach ($data as $key => $value) {
			$query->bindValue(':'.$key, $value);
		}
		
		
		return $query->execute();
	}
	
	/**
	 * @param $table1
	 * @param $table2
	 * @param $id
	 * @return int
	 * @throws \yii\db\Exception
	 */
	public function deleteUser($table1, $table2, $id)
	{
		$query = false;
		
		$sqls = [
			"SELECT id FROM {$table1} WHERE author_id =:id", "DELETE FROM {$table1} WHERE author_id =:id",
			"DELETE FROM {$table2} WHERE id = :id",
		];
		
		foreach ($sqls as $sql) {
			$query = Yii::$app->db->createCommand($sql)->bindValue(':id', $id);
		}
		
		return $query->execute();
	}
}