<?php

namespace app\models;

use PDO;
use Yii;
use yii\base\Model;
use yii\base\StaticInstanceTrait;
use yii\db\Query;

class News extends Model
{
	use StaticInstanceTrait;
	
	/**@var $all array */
	private $all;
	
	/**@var $one array */
	private $one;
	
	/**@var $name string */
	public $name;
	
	/**@var $description string */
	public $description;
	
	/**@var $isActive string */
	public $isActive;
	
	/**@var $updateNews bool*/
	private $updateNews;
	
	/**@var $addNews bool*/
	private $addNews;
	
	/**@var $deleteNews bool*/
	private $deleteNews;
	
	/**@var $inactiveNews array*/
	private $inactiveNews;
	
	/**@var int*/
	private $countInactive;
	
	/**
	 * @return array
	 * @throws \yii\db\Exception
	 */
	public function getAllNews()
	{
		return $this->all = Yii::$app->db->createCommand(
			"SELECT news.id, news.name, news.description, news.is_active AS active_news,
			news.news_date, news.created_at AS news_create_date, news.updated_at AS news_updated,
			author_id, u.first_name, u.last_name, u.email, u.type, u.created_at AS user_created_at,
			u.updated_at AS user_updated_at FROM news INNER JOIN user u ON author_id = u.id ORDER BY news.created_at DESC"
		)->queryAll(PDO::FETCH_OBJ);
	}
	
	/**
	 * @param $table
	 * @param $id
	 * @return array
	 * @throws \yii\db\Exception
	 */
	public function getInactiveNews($table, $id)
	{
		return $this->inactiveNews = Yii::$app->db->createCommand("
			SELECT * FROM {$table} WHERE is_active = 'no' AND author_id=:author_id
		")->bindValues(['author_id' => $id])->queryAll(PDO::FETCH_OBJ);
	}
	
	
	/**
	 * @param $table
	 * @param $id
	 * @return array
	 * @throws \yii\db\Exception
	 */
	public function countInactive($table, $id)
	{
		return $this->countInactive = Yii::$app->db->createCommand("
			SELECT  COUNT(*) posts FROM {$table} WHERE is_active = 'no' AND author_id=:author_id
		")->bindValues(['author_id' => $id])->queryAll(PDO::FETCH_OBJ);
	}
	
	/**
	 * @param $id
	 * @return array|false
	 * @throws \yii\db\Exception
	 */
	public function getOne($id)
	{
		return $this->one = Yii::$app->db->createCommand("SELECT * FROM news WHERE id=:id")->bindValues(
			['id' => $id]
		)->queryOne(PDO::FETCH_OBJ);
	}
	
	/**
	 * @param $table string
	 * @param array $data
	 * @return bool|int
	 * @throws \yii\db\Exception
	 */
	public function updateNews($table, array $data)
	{
		$this->updateNews = Yii::$app->db->createCommand("UPDATE {$table} SET name=:name, description=:description,
                 	is_active=:is_active,
                					created_at = CURRENT_TIMESTAMP(), updated_at = CURRENT_TIMESTAMP() WHERE id=:id");
		
		foreach ($data as $key => $value) {
			$this->updateNews->bindValue(':'.$key, $value);
		}
		
		
		return $this->updateNews->execute();
	}
	
	/**
	 * @param $table
	 * @param array $data
	 * @return bool
	 * @throws \yii\db\Exception
	 */
	public function addNews($table, array $data)
	{
		$this->addNews = Yii::$app->db->createCommand("
			INSERT INTO {$table} SET name=:name, description=:description, is_active=:is_active,
                            news_date = CURDATE(), created_at = CURRENT_TIMESTAMP(), updated_at = CURRENT_TIMESTAMP(),
                            																		author_id=:author_id
		");
		
		foreach ($data as $key => $value) {
			$this->addNews->bindValue(':'.$key, $value);
		}
		
		$this->addNews->execute();
		
		return false;
	}
	
	/**
	 * @param $table
	 * @param $id
	 * @return int
	 * @throws \yii\db\Exception
	 */
	public function deleteNews($table, $id)
	{
		return $this->deleteNews = Yii::$app->db->createCommand()->delete($table, 'id =:id', ['id' => $id])->execute();
	}
	
	/**
	 * @return array|false
	 * @throws \yii\db\Exception
	 */
	public function getDummyText()
	{
		return Yii::$app->db->createCommand("SELECT * FROM dummy_text")->queryOne(PDO::FETCH_OBJ);
	}
}
