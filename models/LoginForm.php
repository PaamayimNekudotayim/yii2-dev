<?php

namespace app\models;

use app\controllers\AppSessionController;
use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class LoginForm extends Model
{
	public $login;
	public $password;
	public $rememberMe = true;
	public $type = false;
	public $_user = false;
	
	/**
	 * @return array the validation rules.
	 */
	public function rules()
	{
		return [
			// username and password are both required
			[['username', 'password'], 'required'],
			// rememberMe must be a boolean value
			['rememberMe', 'boolean'],
			// password is validated by validatePassword()
			['password', 'validatePassword'],
		];
	}
	
	/**
	 * @param bool $type
	 * @return bool
	 * @throws \yii\db\Exception
	 */
	public function login($type = false)
	{
		$login = null;
		$password = null;
		if (isset($_POST['LoginForm']['login']) && isset($_POST['LoginForm']['password'])) {
			$login = !empty(trim($_POST['LoginForm']['login'])) ? trim($_POST['LoginForm']['login']) : null;
			$password = !empty(trim($_POST['LoginForm']['password'])) ? trim($_POST['LoginForm']['password']) : null;
		}
		
		if ($login && $password) {
			/**@var User $user*/
			$user = User::findByUsername($type);
			if ($user) {
				if ($this->validatePassword($password, $user->password)) {
					foreach ($user as $key => $value) {
						if ($key != 'password') {
							AppSessionController::instance()->set($key, $value);
						}
					}
					
					return true;
				}
			}
		}
		
		return false;
	}
	
	
	/**
	 * @param $password
	 * @param $userPassword
	 * @return bool
	 * @throws \yii\db\Exception
	 */
	public function validatePassword($password, $userPassword)
	{
		
		if (!$this->hasErrors()) {
			$user = $this->getUser();
			if (!$user || !User::instance()->validatePassword($password, $userPassword)) {
				return false;
			}
			
			return true;
			
		}
		
		return false;
	}
	
	
	/**
	 * @return array|false|mixed
	 * @throws \yii\db\Exception
	 */
	public function getUser()
	{
		if ($this->_user === false) {
			$this->_user = User::findByUsername($this->type);
		}
		
		return $this->_user;
	}
}
