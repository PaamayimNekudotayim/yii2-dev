<?php

namespace app\models;


use PDO;
use Yii;
use yii\base\StaticInstanceTrait;

class UserRegister
{
	use StaticInstanceTrait;
	
	/**@var $login string*/
	public $login;
	
	/**@var $email string*/
	public $email;
	
	
	/**
	 * @param $table
	 * @return array|false
	 * @throws \yii\db\Exception
	 */
	public function checkLoggedInUser($table)
	{
		$query = Yii::$app->db->createCommand("SELECT * FROM {$table} WHERE email=:email OR login=:login LIMIT 1")
			->bindValues(['email' => trim(filter_var($_POST['RegisterForm']['email'], FILTER_SANITIZE_EMAIL)), 'login' =>
										trim(filter_var($_POST['RegisterForm']['login'], FILTER_SANITIZE_STRING))])
			->queryOne(PDO::FETCH_OBJ);
		
		return $query;
	}
	
	
	/**
	 * @param $table
	 * @param array $data
	 * @throws \yii\db\Exception
	 */
	public static function insertUser($table, array $data)
	{
		Yii::$app->db->createCommand()->insert("{$table}", $data)->execute();
	}
}