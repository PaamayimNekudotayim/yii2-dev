<?php

use app\controllers\AppSessionController;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use yii\widgets\Menu;

/* @var $this yii\web\View */

$this->title = 'Awesome News';
$sessionUser = [];
if (!empty(AppSessionController::instance()->get('user'))) {
	$sessionUser = AppSessionController::instance()->get('user');
}
?>
<div class="site-index">
    <div class="row">
		<?php if (!empty($news)) : ?>
			<?php foreach ($news as $post) : ?>
				<?php if ($post->active_news == 'no') : continue; ?><?php endif; ?>
                <div class="col-sm-6 col-md-4">
                    <div class="thumbnail">
                        <div class="caption">
                            <h3 class="modal-title text-center">
                                <strong><?= Html::decode($post->name) ?></strong></h3>
                            <p><?= Html::decode($post->description) ?></p>
							<?php if (isset($sessionUser)) : ?>
								<?php if (!empty($sessionUser['id'])) : ?>
									<?php if ($post->author_id == $sessionUser['id']) : ?>
                                        <p>
                                            <a href="<?= Url::toRoute(['news/edit', 'post_id' => Html::encode($post->id)]); ?>"
                                               class="btn btn-primary"
                                               role="button"><?= Html::encode('Edit Post') ?></a>
                                            
                                            <a href="javascript:void(0)"
                                               class="btn btn-default delete_post" id="delete-post"
                                               data-id="<?= Html::encode($post->id) ?>"
                                               role="button"><?= Html::encode('Delete'); ?></a>
                                            <span data-id="<?= Html::encode($post->id) ?>"
                                                  class="glyphicon glyphicon-remove delete_post text-danger del-item"
                                                  aria-hidden="true"></span>
                                        </p>
									<?php endif; ?>
								<?php endif; ?>
							<?php endif; ?>
                            <p><strong><?= Html::encode('Date: ') ?><span
                                            class="date"><?= Html::encode($post->news_date) ?></span></strong></p>
                            <p>
                                <strong><?= Html::encode('Author: ') ?><?= Html::decode($post->first_name . ' ' . $post->last_name) ?></strong>
                            </p>

                        </div>
                    </div>
                </div>
			<?php endforeach; ?>
        <?php else : ?>
            <?php if (!empty($dummyText)) : ?>
            <div class="row">
                <h1 class="text-center"><?= Html::encode($dummyText->title); ?></h1>
                <blockquote><?= Html::encode('To add new posts, please login or register.') ?></blockquote>
                <div class="thumbnail">
                    <p><?= Html::encode($dummyText->content); ?></p>
                </div>
            </div>
            <?php endif; ?>
		<?php endif; ?>
    </div>
</div>
