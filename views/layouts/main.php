<?php

/* @var $this \yii\web\View */

/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\web\View;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\controllers\AppSessionController;

AppAsset::register($this);

if (null !== AppSessionController::instance()->get('user')) {
	$user = AppSessionController::instance()->get('user');
	
	$inactive = \app\models\News::instance()->countInactive('news', $user['id']);
}

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
	<?php $this->head() ?>
    <?php $this->registerCssFile('/assets/css/sweetalert2.min.css') ?>
    <!--ADMIN-->
    <?php if (Yii::$app->controller->id == 'admin/user'): ?>
        <?php $this->registerCssFile('/assets/bower_components/bootstrap/dist/css/bootstrap.min.css') ?>
        <?php $this->registerCssFile('/assets/bower_components/font-awesome/css/font-awesome.min.css') ?>
        <?php $this->registerCssFile('/assets/bower_components/Ionicons/css/ionicons.min.css') ?>
        <?php $this->registerCssFile('/assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') ?>
        <?php $this->registerCssFile('/assets/dist/css/AdminLTE.min.css') ?>
        <?php $this->registerCssFile('/assets/dist/css/skins/_all-skins.min.css') ?>
        <?php $this->registerCssFile('/assets/plugins/iCheck/square/blue.css') ?>
        <link rel="stylesheet"
              href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <?php endif; ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
	<?php
	NavBar::begin([
		'brandLabel' => Yii::$app->name,
		'brandUrl' => Yii::$app->homeUrl,
		'options' => [
			'class' => 'navbar-inverse navbar-fixed-top',
		],
	]);
	echo Nav::widget([
		'options' => ['class' => 'navbar-nav navbar-right'],
		'items' => [
			['label' => 'Home', 'url' => ['/site/index']],
			
			empty($user) ? (
				'<li>' . Nav::widget(
					[
						'options' => ['class' => 'navbar-nav navbar-right'],
						'items' => [
							['label' => 'Login', 'url' => ['/user/login']],
						    ['label' => 'Register', 'url' => ['/user/register']]
						]
					]
				) .
                '</li>'
			)
				: (
				'<li>'
				. Html::beginForm(['/user/logout'], 'post')
				. Html::submitButton(
					'Logout (' . $user['first_name'] . ')',
					['class' => 'btn btn-link logout']
				)
				. Html::endForm()
				. '</li>' .
				
				'<li>' . Nav::widget(
					[
						'options' => ['class' => 'navbar-nav navbar-right'],
						'items' => [
							['label' => 'Add New Post', 'url' => ['/news/add']],
                            ['label' => 'Inactive Posts (' . $inactive[0]->posts . ')', 'url' => ['/news/inactive']],
                            ['label' => 'Admin Login', 'url' => ['/admin/user/login']],
						]
					]
				) .
                '</li>'
			)
		]
	]);
	NavBar::end();
	?>

    <div class="container">
		<?= Breadcrumbs::widget([
			'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
		]) ?>
		<?= Alert::widget() ?>
		<?= $content ?>
    </div>
</div>
<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; Awesome News <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>


    <?php $this->registerJsFile("/assets/js/sweetalert2.min.js") ?>
    <?php $this->registerJsFile("/assets/bower_components/bootstrap/dist/js/bootstrap.min.js") ?>
    <?php $this->registerJsFile("/assets/plugins/iCheck/icheck.min.js") ?>
<!--Admin-->
<?php if (Yii::$app->controller->id == 'admin/user') : ?>
	<?php $this->registerJs("
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
        });
    ") ?>
<?php endif; ?>

<?php $this->endBody() ?>
<?php $this->registerJsVar('
    postRedirect
', '' . (isset($_SERVER['HTTPS']) ? 'https' : 'http' ). "://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] . '') ?>
<?php $this->registerJsFile("/assets/js/awesome.js") ?>
</body>
</html>
<?php $this->endPage() ?>