<?php

use app\controllers\AppSessionController;
use yii\bootstrap\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Admin Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="login-box">
    <div class="login-logo">
        <b>Admin</b>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg"><?= Html::encode('Sign in to start your session') ?></p>
		<?php $form = ActiveForm::begin([
			'id' => 'login-form',
		]); ?>
		<?php if(true == Yii::$app->session->getFlash('error')) : ?>
            <div class="alert alert-danger alert-dismissible">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <div class="alert alert-danger" id="alert">
                    <h4><i class="icon fa fa-ban"></i> Error!</h4>
                    <p class="text-center"><?= Yii::$app->session->getFlash('error') ?></p>
                    <p class="text-center"><a href="/"><?= Html::encode('Go To Posts') ?></a></p>
					<?php Yii::$app->session->remove('error'); ?>
                </div>
            </div>
		<?php endif; ?>
        <div class="form-group has-feedback">
			<?= $form->field($model, 'login')->textInput(['autofocus' => true]) ?>
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
			<?= $form->field($model, 'password')->passwordInput() ?>
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>

        <div class="form-group">
            <div class="col-lg-offset-1 col-lg-11">
				<?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            </div>
        </div>
		
		<?php ActiveForm::end(); ?>



    </div>
    <!-- /.login-box-body -->
</div>
<!-- /.login-box -->