<?php  
/**
 * @var $author array
 */

use yii\bootstrap\Html;

$this->title = 'Edit User';
?>
<div class="box box-primary">
	<?php if (isset($author)) : ?>
		<div class="box-header with-border">
			<h3 class="box-title"><?= Html::encode('Edit User Data') ?> &nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="/admin/user/admin"><?= Html::encode('All users'); ?></a>
            </h3>
			<p><b><?= Html::encode('User Role (Type):') ?> <?php echo  ($author->type == '2' ? 'User' : 'Admin');  ?></b></p>
		</div>
		<form method="post" action="admin/user/update" role="form">
			<div class="box-body">
				<?php foreach ($author as $user => $value) : ?>
					<div class="form-group">
						<label for="<?= Html::encode($user) ?>">
							<?= Html::encode(ucwords(str_replace('_', ' ', $user))); ?>
						</label>
						<input type="text" class="form-control" name="<?= Html::encode($user) ?>"
						       id="<?= Html::encode($user) ?>" value="<?= Html::encode($value); ?>">
					</div>
				<?php endforeach; ?>
                <input type="hidden" name="nonce" value="<?= Yii::$app->request->csrfToken; ?>" />
			</div>
			<div class="box-footer">
				<button type="submit" class="btn btn-primary"><?= Html::encode('Submit') ?></button>
			</div>
		</form>
	<?php else : ?>
		Nothing to view :(
	<?php endif; ?>
</div>