<?php
/**
 * @var $allUsers array
 */

use yii\bootstrap\Html;

$this->title = 'Admin';
?>

<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title"><?= Html::encode('All Users'); ?></h3>
</div>
<!-- /.box-header -->
<div class="box-body">
	<table id="example2" class="table table-bordered table-hover">
		<thead>
		<tr>
			<th><?= Html::encode('First Name'); ?></th>
			<th><?= Html::encode('Last Name'); ?></th>
			<th><?= Html::encode('Email'); ?></th>
			<th><?= Html::encode('Created At'); ?></th>
			<th><?= Html::encode('Type'); ?></th>
			<th><?= Html::encode('Actions'); ?></th>
		</tr>
		</thead>
		<tbody>
		<?php if (isset($allUsers)) : ?>
			<?php foreach ($allUsers as $user) : ?>
				<tr>
					<td><strong><?= Html::encode($user->first_name); ?></strong></td>
					<td><strong><?= Html::encode($user->last_name); ?></strong></td>
					<td><a href="mailto:<?= Html::encode($user->email); ?>"><?= Html::encode($user->email); ?></td>
					<td><?= Html::encode($user->created_at); ?></td>
					<td><?= Html::encode(($user->type == '1' ? 'Admin' : 'User')); ?></td>
					<td><a id="admin-delete-user" data-id="<?= Html::encode($user->id) ?>"
					       href="javascript:void(0)">
							<?= Html::encode('Delete') ?></a> |
						<a id="admin-edit-user" href="admin/user/edit?userId=<?= Html::encode($user->id) ?>""><?= Html::encode('Edit') ?></a></td>
				</tr>
			<?php endforeach; ?>
		<?php endif; ?>
		</tbody>
	</table>
</div>
</div>
</div>
</div>