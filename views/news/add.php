<?php

use app\controllers\AppSessionController;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Add new Post';
$this->params['breadcrumbs'][] = $this->title;

$user = [];
if (null !== AppSessionController::instance()->get('user')) {
	$user = AppSessionController::instance()->get('user');
} else {
	$user['id'] = null;
}
?>

<?php if (!empty($model)) :
	
	
	$form = ActiveForm::begin([
		'id' => 'login-form',
		'options' => [
			'class' => 'form-horizontal',
		],
	]);
	?>
	<div class="row">
		<h1 class="text-center edit-post">Add New Post</h1>
		<div class="form-group">
			<div class="col-sm-8 col-sm-offset-2">
				<?= $form->field($model, 'name')->input('text', [
					'class' => 'form-control single-post',
				]) ?>
			</div>
		</div>
		
		<div class="form-group">
			<div class="col-sm-8 col-sm-offset-2">
				<?= $form->field($model, 'description')->textarea([
					'rows' => '10',
				]); ?>
			</div>
		</div>
		
		<div class="form-group">
			<div class="col-sm-8 col-sm-offset-2">
				<?= $form->field($model, 'isActive')->dropdownList(
					[
						'yes' => 'Yes',
						'no' => 'No'
					]
				);
				?>
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-8 col-sm-offset-2">
				<input type="hidden" name="authorId" value="<?= Html::encode($user['id']); ?>" />
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-8 col-sm-offset-2">
				<input type="hidden" name="nonce" value="<?= Yii::$app->request->csrfToken; ?>" />
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-8 col-sm-offset-2">
				<?= Html::submitButton('Add Post', ['class' => 'btn btn-primary']) ?>
			</div>
		</div>
	</div>
	<?php ActiveForm::end() ?>
<?php endif; ?>
