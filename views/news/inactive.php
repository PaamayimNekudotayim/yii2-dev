<?php

use yii\bootstrap\Html;

?>
<div class="container">
	<div class="row">
		<h1 class="text-center"><?= Html::encode('All Inactive Posts') ?></h1>
		<p class="text-center"><a href="/" class="btn btn-primary" role="button"><?= Html::encode(' Go to posts') ?></a></p>
		<?php if (isset($model)) : ?>
			<?php if (!empty($model)) : ?>
				<?php if (is_array($model)) :?>
					<?php foreach ($model as $news) : ?>
						<?php if ($user['id'] !== $news->author_id) : continue; ?>
						<?php endif; ?>
						<div class="col-sm-6 col-md-4">
							<div class="thumbnail">
								<div class="caption">
									<h3 class="modal-title text-center"><strong><?= Html::decode($news->name) ?></strong></h3>
									<p><?= Html::decode($news->description) ?></p>
									<?php if (isset($user)) : ?>
										<?php if (!empty($user['id'])) : ?>
											<?php if ($news->author_id == $user['id']) :  ?>
												<p>
													<a href="index.php?r=news/edit&post_id=<?= Html::encode($news->id) ?>" class="btn btn-primary" role="button">
														<?= Html::encode('Edit') ?></a>
													<a href="javascript:void(0)"
													   class="btn btn-default delete_post" id="delete-post" data-id="<?= Html::encode($news->id) ?>"
													   role="button"><?= Html::encode('Delete') ?></a>
												</p>
											<?php endif; ?>
										<?php endif; ?>
									<?php endif;?>
								</div>
							</div>
						</div>
					<?php endforeach; ?>
				<?php endif; ?>
			<?php else: ?>
				<h1 class="text-center"><?= Html::encode('No posts found :(') ?></h1>
			<?php endif; ?>
		
		<?php endif; ?>
	
	</div>
</div>