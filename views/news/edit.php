<?php

/**
 * @var $post array
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Edit Post';
$this->params['breadcrumbs'][] = $this->title;

$isActive = $post->is_active == 'yes' ? [
	'yes' => 'Yes',
	'no' => 'No',
] : [
	'no' => 'No',
	'yes' => 'Yes'
];
?>
<?php if (!empty($model)) :
	
	
	$form = ActiveForm::begin([
		'id' => 'login-form',
		'options' => [
			'class' => 'form-horizontal',
		],
	]);
	?>
    <div class="row">
        <h1 class="text-center edit-post">Edit Post</h1>
        <div class="form-group">
            <div class="col-sm-8 col-sm-offset-2">
				<?= $form->field($model, 'name')->input('text', [
					'value' => $post->name,
					'class' => 'form-control single-post',
				]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-8 col-sm-offset-2">
				<?= $form->field($model, 'description')->textarea([
					'rows' => '10', 'value' => $post->description
				]); ?>
            </div>
        </div>
        
        <div class="form-group">
            <div class="col-sm-8 col-sm-offset-2">
                <?= $form->field($model, 'isActive')->dropdownList(
	                $isActive
                );
                ?>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-8 col-sm-offset-2">
                <input type="hidden" name="update" value="<?= Html::encode($post->id); ?>" />
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-8 col-sm-offset-2">
                <input type="hidden" name="nonce" value="<?= Yii::$app->request->csrfToken; ?>" />
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-8 col-sm-offset-2">
				<?= Html::submitButton('Update', ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
    </div>
	<?php ActiveForm::end() ?>
<?php else : ?>
    <div class="error-summary"><?= Html::encode('No post found to edit :('); ?></div>
<?php endif; ?>
