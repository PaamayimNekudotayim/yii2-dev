<?php

namespace app\controllers;


use app\models\News;
use stdClass;
use Yii;
use yii\db\Exception;
use yii\web\Controller;

class NewsController extends Controller
{
	/**
	 * @param $action
	 * @return bool
	 * @throws \yii\web\BadRequestHttpException
	 */
	public function beforeAction($action)
	{
		$this->enableCsrfValidation = false;
		return parent::beforeAction($action);
	}
	
	/**
	 * @return string
	 * @throws \yii\db\Exception
	 */
	public function actionEdit()
	{
		$model = new News();
		
		$post = new StdClass();
		
		if (isset($_GET['post_id'])) {
			$post = $model->getOne(strip_tags(trim($_GET['post_id'])));
		} else {
			$post->name = 'No Post';
			$post->description = 'No Description';
		}
		
		
		if (isset($_POST['update']) && $_POST['update'] == strip_tags(trim($_GET['post_id']))) {
			
			$news = $_POST['News'];
			if (!isset($_POST['nonce'])) {
				exit('Abort!');
			}
			
			if (Yii::$app->request->getCsrfToken($_POST['nonce']) === false) {
				exit('Coś poszło nie tak :(. Try again later');
			}
			
			$data = [
				'id' => strip_tags(htmlspecialchars($_POST['update'])),
				'name' => strip_tags(htmlspecialchars($news['name'], ENT_QUOTES, 'UTF-8')),
				'description' => strip_tags(htmlspecialchars($news['description'], ENT_QUOTES, 'UTF-8')),
				'is_active' => strip_tags($news['isActive']),
			];
			
			if (News::instance()->updateNews('news', $data)) {
				AppSessionController::instance()->setFlash('success', 'Post has been updated')
																					->get('success');
			} else {
				AppSessionController::instance()->setFlash('error', 'Oops! Something went wrong :(')
					->get('error');
			}
			
			
			return $this->refresh();
		}
		
		return $this->render('edit', [
			'model' => $model,
			'post' => $post
		]);
	}
	
	/**
	 * @return string|\yii\web\Response
	 * @throws Exception
	 */
	public function actionAdd()
	{
		$model = new News();
		
		if (isset($_POST['authorId'])) {
			if (!isset($_POST['nonce'])) {
				exit('Abort!');
			}
			
			if (Yii::$app->request->getCsrfToken($_POST['nonce']) === false) {
				exit('Coś poszło nie tak :(. Try again later');
			}
			
			$news = $_POST['News'];
			
			$data = [
				'name' => strip_tags(htmlspecialchars($news['name'], ENT_QUOTES, 'UTF-8')),
				'description' => strip_tags(htmlspecialchars($news['description'], ENT_QUOTES, 'UTF-8')),
				'author_id' => strip_tags(htmlspecialchars($_POST['authorId'])),
				'is_active' => strip_tags(htmlspecialchars($news['isActive'], ENT_QUOTES, 'UTF-8')),
			];
			
			News::instance()->addNews('news', $data);
			
			AppSessionController::instance()->setFlash('success', 'Post has been added')->get('success');
			
			return $this->refresh();
		}
		
		return $this->render('add', [
			'model' => $model,
		]);
	}
	
	/**
	 * @return false|string
	 * @throws Exception
	 */
	public function actionDelete()
	{
		if(isset($_POST['delete_post'])) {
			
			$res = [
				'status' => 'success',
				'message' => 'Post was deleted',
				'redirect' => isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : Yii::$app->getHomeUrl(),
			];
			
			News::instance()->deleteNews('news', $_POST['delete_post']);
			
			
			echo json_encode($res);
		}
		
		exit();
	}
	
	/**
	 * @return string
	 * @throws Exception
	 */
	public function actionInactive()
	{
		$user = null;
		if (null !== AppSessionController::instance()->get('user')) {
			$user = AppSessionController::instance()->get('user');
		}
		
		$model = News::instance()->getInactiveNews('news', $user['id']);
		
		return $this->render('inactive', [
			'model' => $model,
			'user' => $user,
		]);
	}
}