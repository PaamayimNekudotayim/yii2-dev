<?php
/**
 * @Dev @vasilguruli.
 * PHP is Awesome. Unfortunately life isn't sometimes,
 * but if you don't fall down then you'll never learn how to get up.
 */

namespace app\controllers\admin;


use app\controllers\AppSessionController;
use app\models\LoginForm;
use app\models\News;
use app\models\User;
use Yii;
use yii\web\Controller;

class UserController extends Controller
{
	/**
	 * @param $action
	 * @return bool
	 * @throws \yii\web\BadRequestHttpException
	 */
	public function beforeAction($action)
	{
		$this->enableCsrfValidation = false;
		return parent::beforeAction($action);
	}
	
	/**
	 * @return string|\yii\web\Response
	 * @throws \yii\db\Exception
	 */
	public function actionLogin()
	{
		$model = new LoginForm();
		
		if (AppSessionController::instance()->get('user') !== null) {
			
			if ($model->load(Yii::$app->request->post())) {
				if (!$model->login(true)) {
					Yii::$app->session->setFlash('error',
						'You have no permissions to access admin area.');
					return $this->render('login', [
						'model' => $model,
					]);
				}
			}
			
			if (User::instance()->isAdmin()) {
				return $this->redirect(['/admin/user/admin']);
			} else {
				return $this->render('login', [
					'model' => $model,
				]);
			}
			
		}
		
		return $this->render('login', [
			'model' => $model,
		]);
	}
	
	/**
	 * @return string
	 * @throws \yii\db\Exception
	 */
	public function actionAdmin()
	{
		$this->layout = 'admin';
		
		if (null == AppSessionController::instance()->get('user') ||
			AppSessionController::instance()->get('user')['type'] !== '1'
		) {
			return $this->goHome();
		}
		
		$allUsers = User::instance()->getRegisteredUsersIfAny('user');
		
		return $this->render('admin', [
			'allUsers' => $allUsers,
		]);
	}
	
	/**
	 * @return string
	 * @throws \yii\db\Exception
	 */
	public function actionEdit()
	{
		$this->layout = 'admin';
		
		if (null == AppSessionController::instance()->get('user') ||
			AppSessionController::instance()->get('user')['type'] !== '1'
		) {
			return $this->goHome();
		}
		
		if (isset($_GET['userId'])) {
			$id = filter_var($_GET['userId'], FILTER_SANITIZE_STRING);
			
			$author = User::instance()->selectAuthorToEdit('user',(int)$id);
			
			return $this->render('edit', [
				'author' => $author,
			]);
		}
		
		return $this->goHome();
	}
	
	/**
	 * @throws \yii\db\Exception
	 */
	public function actionUpdate()
	{
		if (isset($_POST)) {
			
			if (!isset($_POST['nonce'])) {
				exit('Abort!');
			}
			
			if (Yii::$app->request->getCsrfToken($_POST['nonce']) === false) {
				exit('Coś poszło nie tak :(. Try again later');
			}
			
			if (!empty($_POST['id'])) {
				$email = $_POST['email'];
				if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
					Yii::$app->session->setFlash('error', 'Email is incorrect');
					$this->_redirect();
				}
				
				$data = [
					'id' => strip_tags(htmlspecialchars($_POST['id'])),
					'first_name' => strip_tags(htmlspecialchars($_POST['first_name'], ENT_QUOTES, 'UTF-8')),
					'last_name' => strip_tags(htmlspecialchars($_POST['last_name'], ENT_QUOTES, 'UTF-8')),
					'email' => strip_tags($email),
					'login' => strip_tags(htmlspecialchars($_POST['login'], ENT_QUOTES, 'UTF-8')),
					'type' => strip_tags(htmlspecialchars($_POST['type'], ENT_QUOTES, 'UTF-8')),
				];
				
				if (!ctype_digit($data['id']) || !ctype_digit($data['type'])) {
					Yii::$app->session->setFlash('error', 'Id or Type is not in correct format. Only digits allowed.');
					$this->_redirect();
				}
				
				if ($data['type'] > 2 || $data['type'] < 1) {
					Yii::$app->session->setFlash('error', 'Type should be 1 or 2');
					$this->_redirect();
				}
				
				if (User::instance()->updateAuthor('user', $data, '1')) {
					Yii::$app->session->setFlash('success', 'User has been updated');
					
				} else {
					Yii::$app->session->setFlash('error', 'Coś poszło nie tak :(');
				}
			}
		}
		
		$this->_redirect();
	}
	
	/**
	 * @throws \yii\db\Exception
	 */
	public function actionDelete()
	{
		if(isset($_POST['delete_author'])) {
			$id = intval(filter_var($_POST['delete_author'], FILTER_SANITIZE_STRING));
			
			$res = [
				'status' => 'success',
				'message' => 'Author was deleted',
				'redirect' => filter_var($_POST['redirect'], FILTER_VALIDATE_URL),
			];
			
			User::instance()->deleteUser('news', 'user', $id);
			
			echo  json_encode($res);
		}
		exit();
	}
	
	public function _redirect()
	{
		$redirect = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : Yii::$app->getHomeUrl();
		
		header("Location: $redirect");
		
		exit();
	}
}