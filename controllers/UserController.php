<?php

namespace app\controllers;

use app\models\LoginForm;
use app\models\RegisterForm;
use app\models\UserRegister;
use Yii;
use yii\web\Controller;

class UserController extends Controller
{
	public function actionLogout()
	{
		AppSessionController::instance()->remove('user');
		
		return $this->goHome();
	}
	
	/**
	 * @return string|\yii\web\Response
	 * @throws \yii\db\Exception
	 */
	public function actionLogin()
	{
		if (AppSessionController::instance()->get('user') === null) {
			$model = new LoginForm();
			if ($model->load(Yii::$app->request->post()) && $model->login()) {
				if ($model->login()) {
					return $this->goBack();
				}
			} else {
				Yii::$app->session->setFlash('error', 'Login or Password incorrect');
			}
			
			return $this->render('login', [
				'model' => $model,
			]);
			
		}
		
		return $this->goHome();
	}
	
	/**
	 * @return string|Response
	 * @throws \yii\base\Exception
	 * @throws \yii\db\Exception
	 */
	public function actionRegister()
	{
		$model = new RegisterForm();
		
		if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			$model->loadData($_POST['RegisterForm']);
			if (!$model->checkUnique()) {
				
				Yii::$app->session->get('error');
				
				return $this->refresh();
			} else {
				$model->attributes['password'] = Yii::$app->getSecurity()->generatePasswordHash($model->attributes['password'],
																												8);
				$model->attributes['type'] = '2';
				$model->attributes['created_at'] = date('Y-m-d H:i:s');
				$model->attributes['updated_at'] = date('Y-m-d H:i:s');
				
				UserRegister::insertUser('user', $model->attributes);
				
				return $this->redirect(['login']);
			}
		}
		
		return $this->render('register', [
			'model' => $model,
		]);
	}
}