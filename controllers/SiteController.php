<?php

namespace app\controllers;

use app\models\News;
use app\models\RegisterForm;
use app\models\Test;
use app\models\User;
use app\models\UserRegister;
use PDO;
use Yii;
use yii\base\Model;
use yii\data\Pagination;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
	
	/**
	 * @param $action
	 * @return bool
	 * @throws \yii\web\BadRequestHttpException
	 */
	public function beforeAction($action)
	{
		$this->enableCsrfValidation = false;
		
		return parent::beforeAction($action);
	}
	/**
	 * {@inheritdoc}
	 */
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'only' => ['logout'],
				'rules' => [
					[
						'actions' => ['logout'],
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'logout' => ['post'],
				],
			],
		];
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function actions()
	{
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
			'captcha' => [
				'class' => 'yii\captcha\CaptchaAction',
				'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
			],
		];
	}
	
	/**
	 * Displays homepage.
	 * @return string
	 * @throws \yii\db\Exception
	 */
	public function actionIndex()
	{
		/**@var $news Model*/
		$news = News::instance()->getAllNews();
		$dummyText = News::instance()->getDummyText();
		
		return $this->render('index', [
			'news' => $news,
			'dummyText' => $dummyText,
		]);
	}
	
	
	/**
	 * Logout action.
	 *
	 * @return Response
	 */
	public function actionLogout()
	{
		AppSessionController::instance()->remove('user');
		
		return $this->goHome();
	}
}
