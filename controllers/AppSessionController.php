<?php
/**
 * @Dev @vasilguruli.
 * PHP is Awesome. Unfortunately life isn't sometimes,
 * but if you don't fall down then you'll never learn how to get up.
 */

namespace app\controllers;


use yii\base\StaticInstanceTrait;
use yii\web\Session;

class AppSessionController extends Session
{
	use StaticInstanceTrait;
	
	public $name = 'user';
	
	/**
	 * @param string $key
	 * @param mixed $value
	 */
	public function set($key, $value)
	{
		$this->open();
		$_SESSION[$this->name][$key] = $value;
	}
	
	/**
	 * @param $key
	 * @param null $defaultValue
	 * @return mixed
	 */
	public function get($key, $defaultValue = null)
	{
		if (isset($_SESSION[$key])) {
			return $_SESSION[$key];
		}
		
		return null;
	}
	
	/**
	 * @param string $key
	 * @return mixed|void
	 */
	public function remove($key)
	{
		$this->open();
		if (isset($_SESSION[$key])) {
			unset($_SESSION[$key]);
		}
	}
	
	/**
	 * @param string $key
	 * @param bool $value
	 * @param bool $removeAfterAccess
	 * @return $this
	 */
	public function setFlash($key, $value = true, $removeAfterAccess = true)
	{
		$counters = $this->get($this->flashParam, []);
		$counters[$key] = $removeAfterAccess ? -1 : 0;
		$_SESSION[$key] = $value;
		$_SESSION[$this->flashParam] = $counters;
		
		return $this;
	}
}