<?php

use yii\db\Migration;

/**
 * Class m190319_095313_table_author
 */
class m190319_095313_table_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190319_095313_table_author cannot be reverted.\n";

        return false;
    }
    
    public function up()
    {
	    $this->createTable('user', [
		    'id' => $this->primaryKey(),
		    'first_name' => $this->string(100)->null(),
		    'last_name' => $this->string(100)->null(),
		    'login' => $this->string(150)->notNull(),
		    'email' => $this->string(255)->null(),
		    'birth_date' => $this->date()->notNull(),
		    'type' => $this->smallInteger(10)->defaultValue(2),
		    'password' => $this->string(255)->notNull(),
		    'created_at' => $this->dateTime()->notNull(),
		    'updated_at' => $this->dateTime()->notNull(),
	    ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');
	
	    $this->insert('user', [
		    'id' => 1,
		    'first_name' => 'Admin',
		    'last_name' => 'User',
		    'login' => 'admin',
		    'email' => 'admin@mail.com',
		    'birth_date' => '1990-12-05',
		    'type' => 1,
		    'password' => password_hash('mhAdmin12', PASSWORD_DEFAULT),
		    'created_at' => date('Y-m-d'),
		    'updated_at' => date('Y-m-d')
	    ]);
    }

    public function down()
    {
        echo "m190319_095313_table_author cannot be reverted.\n";

        return false;
    }
   
}
