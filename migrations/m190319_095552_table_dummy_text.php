<?php

use yii\db\Migration;

/**
 * Class m190319_095552_table_dummy_text
 */
class m190319_095552_table_dummy_text extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190319_095552_table_dummy_text cannot be reverted.\n";

        return false;
    }
    
    public function up()
    {
		$this->createTable('dummy_text', [
			'id' => $this->integer(11)->notNull(),
			'title' => $this->string(150)->notNull(),
			'content' => $this->text(),
		], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');
		
		$this->insert('dummy_text', [
			'id' => 1,
			'title' => 'Awesome Dummy Text',
			'content' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.
								Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,
								when an unknown printer took a galley of type and scrambled it to make a type specimen book .
                                    It has survived not only five centuries, but also the leap into electronic typesetting,
								remaining essentially unchanged . It was popularised in the 1960s with the release of Letraset
								sheets  containing Lorem Ipsum passages, and more recently with desktop publishing software like
								Aldus PageMaker including versions of Lorem Ipsum .'
		]);
    }

    public function down()
    {
        echo "m190319_095552_table_dummy_text cannot be reverted.\n";

        return false;
    }
   
}
