<?php

use yii\db\Migration;

/**
 * Class m190319_095155_table_news
 */
class m190319_095155_table_news extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190319_095155_table_news cannot be reverted.\n";

        return false;
    }
    
    public function up()
    {
	    $this->createTable('news', [
		    'id' => $this->primaryKey(),
		    'name' => $this->string(255)->notNull(),
		    'description' => $this->text(),
		    'is_active' => $this->string(10)->defaultValue('no'),
		    'news_date' => $this->date()->notNull(),
		    'created_at' => $this->dateTime()->notNull(),
		    'updated_at' => $this->dateTime()->notNull(),
		    'author_id' => $this->integer(11)->notNull(),
	    ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');
    }

    public function down()
    {
        echo "m190319_095155_table_news cannot be reverted.\n";

        return false;
    }
    
}
