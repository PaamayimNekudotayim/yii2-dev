$(document).ready(function(){
    // News Delete
    $(document).on('click', '.delete_post', function (){
        var postId = $(this).data('id');
        swal({
            title: 'Delete',
            text: 'Are you sure',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'OK',
            showLoaderOnConfirm: false,
            preConfirm: function() {
                $.ajax({
                    url: '/news/delete',
                    type: 'POST',
                    data: {'delete_post': postId},
                    success: function (res) {
                        swal({
                            title: "Success",
                            text: "Post was deleted!",
                            type: 'success',
                            showConfirmButton: true,
                            showCancelButton: false,
                        });
                        $("[data-id='"+ postId + "']").parent().parent().parent().remove();
                        window.setTimeout(function (){
                            window.location.href = postRedirect;
                        }, 1000);
                    },
                    error: function (){
                        swal({
                            title: "Error",
                            text: "Post was not deleted!",
                            type: 'error',
                            showConfirmButton: false,
                            showCancelButton: false,
                        });
                    }
                })

            },
            allowOutsideClick: false
        });
    });
});